### Project Stack

- Typescript
- React
- Redux Toolkit
- Styled Components
- Formik
- Yup
  
### Project credits

Author: Beniamino Tartarini 

beniamino.tartarini@gmail.com

github: https://github.com/telriot/

website: www.beniamino-tartarini.com

### Realization time

I started the project before the 6-hours indication came out, and worked in several tranches for about 9-10 hours.

Some of the work was done merely out of personal interest, and a simpler version could have been built without any doubt in less than 6 hours.


