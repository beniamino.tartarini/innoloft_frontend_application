import { createSlice } from "@reduxjs/toolkit";
import { IStore, ILayoutInitialState } from "../../types/index";

const initialState: ILayoutInitialState = {
	dropdownIsOpen: false,
};

const layoutSlice = createSlice({
	name: "layout",
	initialState,
	reducers: {
		dropdownToggled: (state, action) => {
			state.dropdownIsOpen = action.payload;
		},
	},
	extraReducers: (builder) => {},
});

export const { dropdownToggled } = layoutSlice.actions;

export const selectDropdownIsOpen = (state: IStore) =>
	state.layout.dropdownIsOpen;

export default layoutSlice.reducer;
