import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { IStore, IDashboardInitialState } from "../../types/index";

const initialState: IDashboardInitialState = {
	activeTab: 0,
	fetchStatus: "idle",
	updateStatus: "idle",
	error: "",
	email: "",
	firstName: "",
	lastName: "",
	city: "",
	street: "",
	number: "",
	country: "",
	postCode: "",
};
interface IUpdateValues {
	firstName?: string;
	lastName?: string;
	city?: string;
	street?: string;
	number?: string;
	postCode?: string;
	country?: string;
	email?: string;
}
const mockupData = {
	email: "ben@gmail.com",
	firstName: "Beniamino",
	lastName: "Tartarini",
	city: "Berlin",
	street: "Oderstrasse",
	number: "24",
	country: "DE",
	postCode: "10247",
};

const waitForMockupData = (data: any, timeout: number) => {
	return new Promise(function (resolve, reject) {
		setTimeout(resolve, timeout);
	}).then(function () {
		return data;
	});
};

export const fetchMyProfile = createAsyncThunk(
	"dashboard/fetchMyProfile",
	async (_, thunkAPI: { dispatch: any; getState: () => any }) => {
		try {
			const response = await waitForMockupData(mockupData, 1000);
			if (!response) {
				return { error: "Something went wrong with our servers" };
			}
			return { profile: response, error: "" };
		} catch (error) {
			console.error(error);
			return { error };
		}
	}
);
export const updateMyProfile = createAsyncThunk(
	"dashboard/updateMyProfile",
	async (
		values: IUpdateValues,
		thunkAPI: { dispatch: any; getState: () => any }
	) => {
		try {
			//API call to upload new data and fetch updated data
			const updateData = {
				email: values.email || mockupData.email,
				firstName: values.firstName || mockupData.firstName,
				lastName: values.lastName || mockupData.lastName,
				city: values.city || mockupData.city,
				street: values.street || mockupData.street,
				number: values.number || mockupData.number,
				country: values.country || mockupData.country,
				postCode: values.postCode || mockupData.postCode,
			};

			const response = await waitForMockupData(updateData, 1000);
			if (!response) {
				return { error: "Something went wrong with our servers" };
			}
			return { profile: response, error: "" };
		} catch (error) {
			console.error(error);
			return { error };
		}
	}
);

const dashboardSlice = createSlice({
	name: "dashboard",
	initialState,
	reducers: {
		tabChanged: (state, action) => {
			state.activeTab = action.payload;
		},
		updateStatusReset: (state) => {
			state.updateStatus = "idle";
			state.error = "";
		},
	},
	extraReducers: (builder) => {
		builder.addCase(fetchMyProfile.pending, (state, action) => {
			state.fetchStatus = "pending";
		});
		builder.addCase(fetchMyProfile.fulfilled, (state, action) => {
			if (action.payload.error) {
				state.error = action.payload.error;
				state.updateStatus = "rejected";
			} else {
				const { profile } = action.payload;
				if (profile !== undefined) {
					state.email = profile!.email;
					state.firstName = profile!.firstName;
					state.lastName = profile!.lastName;
					state.city = profile!.city;
					state.street = profile!.street;
					state.number = profile!.number;
					state.country = profile!.country;
					state.postCode = profile!.postCode;
				}

				state.fetchStatus = "fulfilled";
			}
		});
		builder.addCase(fetchMyProfile.rejected, (state, action) => {
			state.fetchStatus = "rejected";
			state.error = "Something went wrong with our servers";
		});
		builder.addCase(updateMyProfile.pending, (state, action) => {
			state.updateStatus = "pending";
		});
		builder.addCase(updateMyProfile.fulfilled, (state, action) => {
			if (action.payload.error) {
				state.error = action.payload.error;
				state.updateStatus = "rejected";
			} else {
				const { profile } = action.payload;
				if (profile !== undefined) {
					state.email = profile!.email;
					state.firstName = profile!.firstName;
					state.lastName = profile!.lastName;
					state.city = profile!.city;
					state.street = profile!.street;
					state.number = profile!.number;
					state.country = profile!.country;
					state.postCode = profile!.postCode;
				}
				state.updateStatus = "fulfilled";
			}
		});
		builder.addCase(updateMyProfile.rejected, (state, action) => {
			state.updateStatus = "rejected";
			state.error = "Something went wrong with our servers";
		});
	},
});

export const { tabChanged, updateStatusReset } = dashboardSlice.actions;
export const selectActiveTab = (state: IStore) => state.dashboard.activeTab;
export const selectEmail = (state: IStore) => state.dashboard.email;
export const selectError = (state: IStore) => state.dashboard.error;
export const selectFetchStatus = (state: IStore) => state.dashboard.fetchStatus;
export const selectUpdateStatus = (state: IStore) =>
	state.dashboard.updateStatus;

export const selectAddressData = (state: IStore) => {
	const {
		firstName,
		lastName,
		city,
		street,
		number,
		country,
		postCode,
	} = state.dashboard;
	return { firstName, lastName, city, street, number, country, postCode };
};
export default dashboardSlice.reducer;
