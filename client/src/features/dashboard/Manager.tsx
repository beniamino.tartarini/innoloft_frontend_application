import React from "react";
import { useDispatch, useSelector } from "react-redux";
import {
	fetchMyProfile,
	selectActiveTab,
	selectAddressData,
	selectEmail,
	selectFetchStatus,
	selectUpdateStatus,
} from "./dashboardSlice";
import { useWindowWidth } from "@react-hook/window-size";
import Card from "../../components/Card/Card";
import styled from "styled-components";
import TabSelector from "../../components/shared/TabSelector";
import FormMain from "./FormMain";
import FormAdditional from "./FormAdditional";
import LoadingBox from "../../components/shared/LoadingBox";

const Container = styled.div`
	margin: 0 10% 0 0;
	${(props) => props.theme.breakpoints.down("md")} {
		margin: 0 0 3rem 0;
	}
`;
function Manager() {
	const dispatch = useDispatch();
	const activeTab = useSelector(selectActiveTab);
	const addressData = useSelector(selectAddressData);
	const email = useSelector(selectEmail);
	const fetchStatus = useSelector(selectFetchStatus);
	const updateStatus = useSelector(selectUpdateStatus);
	const width = useWindowWidth();

	React.useEffect(() => {
		dispatch(fetchMyProfile());
	}, [dispatch]);
	const isPending = fetchStatus === "pending" || updateStatus === "pending";
	return (
		<Container>
			<Card>
				<TabSelector />
				{activeTab === 0 ? (
					isPending ? (
						<LoadingBox
							boxHeight={width < 400 ? 382 : width < 600 ? 397 : 412}
						/>
					) : (
						<FormMain email={email} />
					)
				) : activeTab === 1 ? (
					isPending ? (
						<LoadingBox
							boxHeight={
								width < 400 ? 700 : width < 600 ? 730 : width < 1340 ? 761 : 533
							}
						/>
					) : (
						<FormAdditional addressData={addressData} />
					)
				) : null}
			</Card>
		</Container>
	);
}

export default Manager;
