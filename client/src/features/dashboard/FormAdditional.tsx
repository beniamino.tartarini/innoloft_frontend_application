import React from "react";
import { useDispatch, useSelector } from "react-redux";
import {
	selectError,
	selectUpdateStatus,
	updateMyProfile,
	updateStatusReset,
} from "./dashboardSlice";
import { Formik, Form, Field, FormikHelpers } from "formik";
import { additionalSchema } from "../../validators";
import AlertBox from "../../components/shared/AlertBox";
import ButtonMain from "../../components/shared/ButtonMain";
import Select from "../../components/shared/Select";
import TextField from "../../components/shared/TextField";
import styled from "styled-components";

interface IValues {
	firstName: string;
	lastName: string;
	city: string;
	street: string;
	number: string;
	postCode: string;
	country: string;
}

const FieldBox = styled.div`
	margin-bottom: 1.25rem;
`;
const AddressBox = styled.div`
	display: grid;
	grid-template-columns: 1fr 8rem;
	gap: 1rem;
	${(props) => props.theme.breakpoints.down("xs")} {
		grid-template-columns: 1fr 4rem;
	}
`;
const CountryBox = styled.div`
	display: grid;
	grid-template-columns: 1fr 11rem 8rem;
	gap: 1rem;
	${(props) => props.theme.breakpoints.down("xl")} {
		display: block;
	}
`;

const countryOptions = [
	{ key: "DE", value: "Germany" },
	{ key: "AT", value: "Austria" },
	{ key: "CH", value: "Switzerland" },
];

function FormAdditional({ addressData }: { addressData: IValues }) {
	const dispatch = useDispatch();
	const handleAlertClose = () => dispatch(updateStatusReset());
	const updateStatus = useSelector(selectUpdateStatus);
	const serverError = useSelector(selectError);
	const updateHadResult =
		updateStatus === "fulfilled" || updateStatus === "rejected";
	const updateSuccessful = updateStatus === "fulfilled";

	return (
		<Formik
			initialValues={{
				firstName: addressData.firstName,
				lastName: addressData.lastName,
				city: addressData.city,
				street: addressData.street,
				number: addressData.number,
				postCode: addressData.postCode,
				country: addressData.country,
			}}
			validationSchema={additionalSchema}
			onSubmit={async (
				values: IValues,
				{ setSubmitting }: FormikHelpers<IValues>
			) => {
				dispatch(updateMyProfile(values));
			}}
		>
			{({ errors, touched }) => (
				<Form>
					{updateHadResult ? (
						<AlertBox
							onClose={handleAlertClose}
							variant={updateSuccessful ? "success" : "warning"}
							message={
								updateSuccessful ? "Data succesfully updated" : serverError
							}
						/>
					) : null}
					<FieldBox>
						<Field
							name="firstName"
							value="firstName"
							component={TextField}
							label="First Name"
							error={
								errors.firstName && touched.firstName ? errors.firstName : ""
							}
						/>
						<Field
							name="lastName"
							value="lastName"
							component={TextField}
							label="Last Name"
							error={errors.lastName && touched.lastName ? errors.lastName : ""}
						/>

						<AddressBox>
							<Field
								name="street"
								value="street"
								component={TextField}
								label="Street"
								error={errors.street && touched.street ? errors.street : ""}
							/>
							<Field
								name="number"
								value="number"
								component={TextField}
								label="N."
								error={errors.number && touched.number ? errors.number : ""}
							/>
						</AddressBox>
						<CountryBox>
							<Field
								name="city"
								value="city"
								component={TextField}
								label="City"
								error={errors.city && touched.city ? errors.city : ""}
							/>
							<Field
								name="country"
								value="country"
								component={Select}
								label="Country"
								options={countryOptions}
								error={errors.country && touched.country ? errors.country : ""}
							/>
							<Field
								name="postCode"
								value="postCode"
								component={TextField}
								label="Postal Code"
								error={
									errors.postCode && touched.postCode ? errors.postCode : ""
								}
							/>
						</CountryBox>
					</FieldBox>

					<ButtonMain large fullWidth>
						Update
					</ButtonMain>
				</Form>
			)}
		</Formik>
	);
}

export default FormAdditional;
