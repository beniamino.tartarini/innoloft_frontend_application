import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Formik, Form, Field, FormikHelpers } from "formik";
import { mainSchema } from "../../validators";
import TextField from "../../components/shared/TextField";
import styled from "styled-components";
import ButtonMain from "../../components/shared/ButtonMain";
import {
	selectError,
	selectUpdateStatus,
	updateMyProfile,
	updateStatusReset,
} from "./dashboardSlice";
import AlertBox from "../../components/shared/AlertBox";

interface IValues {
	email: string;
	password: string;
	passwordConfirmation: string;
}

const FieldBox = styled.div`
	margin-bottom: 1.25rem;
`;

function FormMain({ email }: { email: string }) {
	const dispatch = useDispatch();
	const handleAlertClose = () => dispatch(updateStatusReset());
	const updateStatus = useSelector(selectUpdateStatus);
	const serverError = useSelector(selectError);
	const updateHadResult =
		updateStatus === "fulfilled" || updateStatus === "rejected";
	const updateSuccessful = updateStatus === "fulfilled";

	return (
		<Formik
			initialValues={{
				email: email,
				password: "",
				passwordConfirmation: "",
			}}
			validationSchema={mainSchema}
			onSubmit={async (
				values: IValues,
				{ setSubmitting }: FormikHelpers<IValues>
			) => {
				dispatch(updateMyProfile(values));
			}}
		>
			{({ isValid, values, errors, touched }) => (
				<Form>
					{updateHadResult ? (
						<AlertBox
							onClose={handleAlertClose}
							variant={updateSuccessful ? "success" : "warning"}
							message={
								updateSuccessful ? "Data succesfully updated" : serverError
							}
						/>
					) : null}

					<FieldBox>
						<Field
							name="email"
							value="email"
							component={TextField}
							label="What is your email address?"
							error={errors.email && touched.email ? errors.email : ""}
						/>
						<Field
							type="password"
							name="password"
							value="password"
							component={TextField}
							label="Please choose a password"
							error={errors.password && touched.password ? errors.password : ""}
						/>
						<Field
							type="password"
							name="passwordConfirmation"
							value="passwordConfirmation"
							component={TextField}
							label="Confirm your password"
							error={
								errors.passwordConfirmation && touched.passwordConfirmation
									? errors.passwordConfirmation
									: ""
							}
						/>
					</FieldBox>

					<ButtonMain type="submit" large fullWidth>
						Update
					</ButtonMain>
				</Form>
			)}
		</Formik>
	);
}

export default FormMain;
