import React, { HTMLAttributes } from "react";
import styled from "styled-components";

interface IInputHelperTextProps extends HTMLAttributes<HTMLSpanElement> {
	error?: string;
}

const HelperText = styled.span<IInputHelperTextProps>`
	color: ${(props) => props.theme.palette.danger.main};
	font-size: ${(props) => props.theme.typography.s};
	visibility: ${(props) => (props.error ? "visible" : "hidden")};
	${(props) => props.theme.breakpoints.down("xs")} {
		font-size: ${(props) => props.theme.typography.xs};
	} ;
`;
function InputHelperText({ error }: IInputHelperTextProps) {
	return <HelperText error={error}>{error || "no-error"}</HelperText>;
}

export default InputHelperText;
