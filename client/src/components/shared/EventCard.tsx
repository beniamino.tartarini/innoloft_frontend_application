import React from "react";
import Card from "../Card/Card";
import CardHeader from "../Card/CardHeader";
import CardLink from "../Card/CardLink";
import CardMedia from "../Card/CardMedia";
import CardText from "../Card/CardText";
import challenge from "../../assets/challenge.png";

function EventCard() {
	return (
		<Card>
			<CardHeader>Current Challenge</CardHeader>
			<CardMedia src={challenge} alt="Current challenge" />
			<CardHeader>Connected Care Challenge</CardHeader>
			<CardText>
				Philips wants to improve the lives of billions in the next years to
				come. When you connect data, technology and people seemingly impossible
				things become possible. Patient outcomes improve, hospital staff
				satisfaction goes up and cost of care go down. We want to make
				healthcare better, together.
			</CardText>
			<CardText>
				As we are all well aware, the healthcare landscape is changing rapidly.
				Increased complexity, patient volume, and pressure on devices and system
				integration demands new approaches and thinking in the delivery of high
				quality care to your patients. Making fast, informed decisions by
				leveraging advanced physiological monitoring and clinical informatics
				through connected care is [...]
			</CardText>
			<CardLink>Read more</CardLink>
		</Card>
	);
}

export default EventCard;
