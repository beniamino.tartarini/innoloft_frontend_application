import React from "react";
import styled from "styled-components";
import Loader from "react-loader-spinner";

interface ILoadingBox {
	boxHeight: number;
	spinnerSize?: number;
	color?: string;
	timeout?: number;
}
interface IBoxProps {
	height: number;
}
const Box = styled.div<IBoxProps>`
	height: ${(props) => props.height + "px"};
	display: grid;
	place-items: center;
`;

function LoadingBox({ boxHeight, spinnerSize, color, timeout }: ILoadingBox) {
	return (
		<Box height={boxHeight}>
			<Loader
				type="Grid"
				color={color || "#E4B302"}
				height={spinnerSize || 100}
				width={spinnerSize || 100}
				timeout={timeout || 3000}
			/>
		</Box>
	);
}

export default LoadingBox;
