import React, { HTMLAttributes } from "react";
import { TOptions } from "../../types";
import InputHelperText from "./InputHelperText";
import InputLabel from "./InputLabel";
import styled from "styled-components";

interface ISelectProps {
	options: Array<TOptions>;
	error?: string;
	field?: any;
}
interface IOptionProps extends HTMLAttributes<HTMLOptionElement> {}

const Wrapper = styled.div`
	margin-bottom: 0.25rem;
`;
const SelectInput = styled.select`
	display: block;
	font-size: ${(props) => props.theme.typography.l};
	font-weight: 500;
	color: ${(props) => props.theme.palette.text.primary};
	line-height: 1.35;
	padding: 0.5625rem;
	width: 100%;
	max-width: 100%;
	margin: 0;
	border: 0;
	border-radius: 4px;
	appearance: none;
	background: ${(props) => props.theme.palette.grey.lighter};
	transition: background 0.3s;
	&:-ms-expand {
		display: none;
	}
	&:hover {
		border-color: #888;
	}
	&:focus {
		background: ${(props) => props.theme.palette.grey.mid};
		outline: none;
	}
	${(props) => props.theme.breakpoints.down("sm")} {
		font-size: ${(props) => props.theme.typography.m};
	}
	${(props) => props.theme.breakpoints.down("xs")} {
		font-size: ${(props) => props.theme.typography.s};
	}
`;

const StyledOption = styled.option<IOptionProps>`
	background-color: ${(props) => props.theme.palette.grey.lighter};
	color: ${(props) =>
		props.disabled
			? props.theme.palette.grey.midDark
			: props.theme.palette.text.primary};
`;

function Select({ field, error, options }: ISelectProps) {
	return (
		<Wrapper>
			<InputLabel>Country</InputLabel>
			<SelectInput error={error} {...field}>
				<StyledOption value="" disabled>
					Please pick one
				</StyledOption>
				{options.map((option) => (
					<StyledOption value={option.key}>{option.value}</StyledOption>
				))}
			</SelectInput>
			<InputHelperText error={error} />
		</Wrapper>
	);
}

export default Select;
