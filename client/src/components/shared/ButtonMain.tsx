import React, { ButtonHTMLAttributes } from "react";
import styled from "styled-components";

interface IButton extends ButtonHTMLAttributes<HTMLButtonElement> {
	fullWidth?: boolean;
	slim?: boolean;
	large?: boolean;
}

const StyledButton = styled.button<IButton>`
	display: ${(props) => (props.fullWidth ? "block" : "flex")};
	justify-content: center;
	align-items: center;
	width: ${(props) => (props.fullWidth ? "100%" : "max-content")};
	background-color: ${(props) => props.theme.palette.primary.main};
	color: ${(props) => props.theme.palette.text.white};
	border: none;
	border-radius: 4px;
	padding: ${(props) => (props.slim ? "0.4375rem 0.625rem" : "0.75rem")};
	font-family: Roboto;
	font-size: ${(props) =>
		props.large ? props.theme.typography.m : props.theme.typography.s};
	font-weight: 300;
	letter-spacing: 0.05rem;

	cursor: pointer;
	transition: background-color 0.2s, color 0.2s;
	&:hover,
	&:focus {
		background-color: ${(props) => props.theme.palette.secondary.main};
		color: ${(props) => props.theme.palette.primary.main};
	}
`;

function ButtonMain({ children, slim, large, fullWidth }: IButton) {
	return (
		<StyledButton slim={slim} large={large} fullWidth={fullWidth}>
			{children}
		</StyledButton>
	);
}

export default ButtonMain;
