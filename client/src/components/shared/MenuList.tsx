import React, { HTMLAttributes } from "react";
import styled from "styled-components";
import { StyledIconBase } from "@styled-icons/styled-icon";
import { Home } from "@styled-icons/ionicons-outline/Home";
import { People } from "@styled-icons/ionicons-outline/People";
import { Basket } from "@styled-icons/ionicons-outline/Basket";
import { ColorFilter } from "@styled-icons/ionicons-outline/ColorFilter";
import { Newspaper } from "@styled-icons/ionicons-outline/Newspaper";
import { Rocket } from "@styled-icons/ionicons-outline/Rocket";
import { Business } from "@styled-icons/ionicons-sharp/Business";
import { RadioButtonOff } from "@styled-icons/ionicons-outline/RadioButtonOff";
import { Calendar } from "@styled-icons/ionicons-outline/Calendar";
import { Build } from "@styled-icons/ionicons-outline/Build";

interface IMenuListProps {
	slim?: boolean;
}
interface IMenuItemProps extends HTMLAttributes<HTMLLIElement> {
	slim?: boolean;
}
interface ILinkProps extends HTMLAttributes<HTMLAnchorElement> {
	slim?: boolean;
}

const Container = styled.div`
	display: flex;
	flex-direction: column;
	align-items: flex-start;
`;
const List = styled.ul`
	width: 100%;
`;
const MenuItem = styled.li<IMenuItemProps>`
	display: flex;
	width: 100%;
	padding: ${(props) => (props.slim ? "0.75rem" : "0.9375rem 0.625rem")};
	color: ${(props) =>
		props.slim ? props.theme.palette.grey.lighter : "inherit"};
	border-bottom: ${(props) =>
		props.slim ? "1px solid " + props.theme.palette.grey.mid : "inherit"};
	cursor: pointer;
	${(props) => props.theme.breakpoints.down("xs")} {
		padding: 0.6875rem;
	}
`;
const Link = styled.a<ILinkProps>`
	font-size: ${(props) =>
		props.slim ? props.theme.typography.xs : props.theme.typography.s};
	color: ${(props) => props.theme.palette.text.primary};
	padding: 0 0.9375rem;
`;
const IconStyleWrapper = styled.div`
	${StyledIconBase} {
		color: ${(props) => props.theme.palette.text.primary};
		height: 16px;
		width: 16px;
		${(props) => props.theme.breakpoints.down("xs")} {
			height: 14px;
			width: 14px;
		}
	}
`;

const menuItems = [
	{ icon: <Home />, title: "Home" },
	{ icon: <People />, title: "Member" },
	{ icon: <Basket />, title: "Marketplace" },
	{ icon: <ColorFilter />, title: "Matching" },
	{ icon: <Newspaper />, title: "News" },
	{ icon: <Rocket />, title: "Startups" },
	{ icon: <Business />, title: "Companies" },
	{ icon: <RadioButtonOff />, title: "Ecosystem" },
	{ icon: <Calendar />, title: "Events" },
	{ icon: <Build />, title: "Tools" },
];

function MenuList({ slim }: IMenuListProps) {
	return (
		<IconStyleWrapper>
			<Container>
				<List>
					{menuItems.map((item) => (
						<MenuItem key={`menu-item-${item.title}`} slim={slim}>
							{item.icon}
							<Link slim={slim}>{item.title}</Link>
						</MenuItem>
					))}
				</List>
			</Container>
		</IconStyleWrapper>
	);
}

export default MenuList;
