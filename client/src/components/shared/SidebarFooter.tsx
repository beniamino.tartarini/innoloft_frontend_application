import React, { HTMLAttributes } from "react";
import styled from "styled-components";
import Copyright from "./Copyright";
import { LogoXing } from "@styled-icons/ionicons-solid/LogoXing";
import { LogoFacebook } from "@styled-icons/ionicons-solid/LogoFacebook";
import { LogoLinkedin } from "@styled-icons/ionicons-solid/LogoLinkedin";
import { LogoTwitter } from "@styled-icons/ionicons-solid/LogoTwitter";
import { StyledIconBase } from "@styled-icons/styled-icon";

interface ISidebarFooterProps {
	slim?: boolean;
}
interface IContainer extends HTMLAttributes<HTMLDivElement> {
	slim?: boolean;
}
const Container = styled.div<IContainer>`
	display: flex;
	flex-direction: column;
	align-items: center;
	border-top: ${(props) => "1px solid " + props.theme.palette.grey.mid};
	margin-top: ${(props) => (props.slim ? 0 : "0.9375rem")};
	margin-bottom: ${(props) => (props.slim ? "0.9375rem" : 0)};
	padding-top: ${(props) => (props.slim ? 0 : "0.9375rem")};
`;
const LinkBox = styled.div`
	display: flex;
	justify-content: center;
	flex-wrap: wrap;
`;
const Link = styled.a`
	font-size: ${(props) => props.theme.typography.s};
	color: ${(props) => props.theme.palette.text.tertiary};
	margin: 0.375rem 0.3125rem 0;
`;
const IconStyleWrapper = styled.div`
	${StyledIconBase} {
		color: ${(props) => props.theme.palette.text.tertiary};
		margin: 0 0.25rem;
	}
`;
const SocialLink = styled.a``;
const footerLinks = [
	{ title: "Contact", to: "https://www.innoloft.com" },
	{ title: "Data Privacy", to: "https://www.innoloft.com" },
	{ title: "Imprint", to: "https://www.innoloft.com" },
	{ title: "Terms of use", to: "https://www.innoloft.com" },
	{ title: "Blog", to: "https://www.innoloft.com" },
];

const socialLinks = [
	{ icon: <LogoFacebook size={20} />, to: "https://www.facebook.com" },
	{ icon: <LogoTwitter size={20} />, to: "https://www.twitter.com" },
	{ icon: <LogoXing size={20} />, to: "https://www.xing.com" },
	{ icon: <LogoLinkedin size={20} />, to: "https://www.linkedin.com" },
];
function SidebarFooter({ slim }: ISidebarFooterProps) {
	return (
		<Container slim={slim}>
			<LinkBox>
				{footerLinks.map((link) => (
					<Link key={`footer-link-${link.title}`}>{link.title}</Link>
				))}
			</LinkBox>

			<Copyright />
			<IconStyleWrapper>
				<LinkBox>
					{socialLinks.map((link) => (
						<SocialLink href={link.to}>{link.icon}</SocialLink>
					))}
				</LinkBox>
			</IconStyleWrapper>
		</Container>
	);
}

export default SidebarFooter;
