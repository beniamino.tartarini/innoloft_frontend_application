import React, { HTMLAttributes } from "react";
import styled from "styled-components";
interface ILabelProps extends HTMLAttributes<HTMLDivElement> {}

const Label = styled.div`
	font-size: 1.25rem;
	font-weight: 700;
	color: ${(props) => props.theme.palette.text.label};
	margin-bottom: 1.25rem;
	${(props) => props.theme.breakpoints.down("sm")} {
		font-size: ${(props) => props.theme.typography.l};
	}
	${(props) => props.theme.breakpoints.down("xs")} {
		font-size: ${(props) => props.theme.typography.m};
	}
`;

function InputLabel({ children }: ILabelProps) {
	return <Label>{children}</Label>;
}

export default InputLabel;
