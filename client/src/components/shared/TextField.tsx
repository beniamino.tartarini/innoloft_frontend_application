import React, { HTMLAttributes } from "react";
import InputLabel from "./InputLabel";
import InputHelperText from "./InputHelperText";
import PswStrengthIndicator from "./PswStrengthIndicator";
import styled from "styled-components";

interface ITextFieldProps extends HTMLAttributes<HTMLElement> {
	label: string;
	error?: string;
	field?: any;
	type: string;
}
interface IInputFieldProps extends HTMLAttributes<HTMLInputElement> {
	error?: string;
}

const Wrapper = styled.div`
	margin-bottom: 0.25rem;
`;
const InputField = styled.input<IInputFieldProps>`
	background: ${(props) => props.theme.palette.grey.lighter};
	border: 0;
	width: 100%;
	padding: 0.5625rem;
	outline: none;
	font-family: "Open Sans";
	font-size: ${(props) => props.theme.typography.l};
	color: ${(props) => props.theme.palette.text.primary};
	border-bottom: ${(props) =>
		props.error
			? "2px solid " + props.theme.palette.danger.main
			: "2px solid transparent"};
	border-radius: 4px;
	transition: background 0.3s;

	&:focus {
		background: ${(props) => props.theme.palette.grey.mid};
	}
	${(props) => props.theme.breakpoints.down("sm")} {
		font-size: ${(props) => props.theme.typography.m};
	}
	${(props) => props.theme.breakpoints.down("xs")} {
		font-size: ${(props) => props.theme.typography.s};
	}
`;

function TextField({ field, label, error, type }: ITextFieldProps) {
	return (
		<Wrapper>
			<InputLabel>{label}</InputLabel>
			<InputField error={error} type={type} {...field} />
			{field.name === "password" ? (
				<PswStrengthIndicator password={field.value} error={error} />
			) : (
				<InputHelperText error={error}>{error || "no-error"}</InputHelperText>
			)}
		</Wrapper>
	);
}

export default TextField;
