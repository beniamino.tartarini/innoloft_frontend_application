import React, { HTMLAttributes } from "react";
import { CloseCircle } from "@styled-icons/ionicons-outline/CloseCircle";
import styled from "styled-components";

type TAlertVariant = "success" | "warning";
interface IAlertBox extends HTMLAttributes<HTMLDivElement> {
	message: string;
	variant: TAlertVariant;
	onClose: () => any;
}

interface IBoxProps {
	variant: TAlertVariant;
}

const Box = styled.div<IBoxProps>`
	display: flex;
	justify-content: space-between;
	align-items: center;
	border: 0;
	border-radius: 4px;
	padding: 0.625rem;
	margin-bottom: 1rem;
	background: ${(props) =>
		props.variant === "warning"
			? props.theme.palette.danger.light
			: props.variant === "success"
			? props.theme.palette.success.light
			: ""};
`;

const AlertText = styled.span`
	font-size: ${(props) => props.theme.typography.s};
	color: ${(props) => props.theme.palette.text.primary};
	${(props) => props.theme.breakpoints.down("xs")} {
		font-size: ${(props) => props.theme.typography.xs};
	}
`;

const CloseIcon = styled(CloseCircle)`
	color: ${(props) => props.theme.palette.text.primary};
	cursor: pointer;
	transition: color 0.3s;
	&:hover {
		color: ${(props) => props.theme.palette.grey.light};
	}
`;

function AlertBox({ message, variant, onClose }: IAlertBox) {
	return (
		<Box variant={variant}>
			<AlertText>{message}</AlertText>
			<CloseIcon onClick={onClose} size={22} />
		</Box>
	);
}

export default AlertBox;
