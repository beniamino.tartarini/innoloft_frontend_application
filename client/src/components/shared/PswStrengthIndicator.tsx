import React, { HTMLAttributes } from "react";
import styled from "styled-components";
import {
	lowerCaseRegExp,
	upperCaseRegExp,
	numberRegExp,
	specialRegExp,
	lengthRegExp,
} from "../../validators";

type TStrength =
	| "invalid"
	| "poor"
	| "weak"
	| "average"
	| "strong"
	| "bulletproof"
	| undefined;

interface IPswStrengthIndicator {
	password: string;
	error?: string;
}
interface IIndicatorProps extends HTMLAttributes<HTMLElement> {
	strength: TStrength;
	password: string;
	error: boolean;
}
const colors = {
	invalid: "#ff4141",
	poor: "#ef6c00",
	weak: "#fb8c00",
	average: "#00c853",
	strong: "#00897b",
	bulletproof: "#01579b",
};
const Indicator = styled.span<IIndicatorProps>`
	font-size: ${(props) => props.theme.typography.s};
	visibility: ${(props) =>
		props.password || props.error ? "visible" : "hidden"};
	color: ${(props) => (props.strength ? colors[props.strength] : "#ff4141")};
`;

function PswStrengthIndicator({ password, error }: IPswStrengthIndicator) {
	const [strength, setStrength] = React.useState<TStrength>("invalid");

	const calculatePasswordStrength = (password: string) => {
		const regExpArr = [
			lowerCaseRegExp,
			upperCaseRegExp,
			numberRegExp,
			specialRegExp,
			lengthRegExp,
		];
		const strengthArr: Array<TStrength> = [
			"invalid",
			"poor",
			"weak",
			"average",
			"strong",
			"bulletproof",
		];
		let strengthIndex = 0;
		if (lengthRegExp.test(password)) {
			for (let regExp of regExpArr) {
				regExp.test(password) && strengthIndex++;
			}
		}

		return strengthArr[strengthIndex];
	};

	React.useEffect(() => {
		setStrength(calculatePasswordStrength(password));
	}, [password]);

	return (
		<Indicator
			password={password}
			strength={strength}
			error={Boolean(error?.length)}
		>
			{error?.length ? error : `Strength: ${strength}`}
		</Indicator>
	);
}

export default PswStrengthIndicator;
