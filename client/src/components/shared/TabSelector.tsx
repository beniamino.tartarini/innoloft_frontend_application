import React, { HTMLAttributes } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
	selectActiveTab,
	tabChanged,
} from "../../features/dashboard/dashboardSlice";
import styled from "styled-components";

interface ITabProps extends HTMLAttributes<HTMLDivElement> {
	selected: boolean;
	position: "left" | "center" | "right";
}

const Container = styled.div`
	display: flex;
	margin-bottom: 1.25rem;
`;

const StyledTab = styled.div<ITabProps>`
	flex: 1;
	display: flex;
	align-items: center;
	justify-content: center;
	padding: 0.625rem;
	border-radius: ${(props) =>
		props.position === "left"
			? "4px 0 0 4px"
			: props.position === "right"
			? "0 4px 4px 0"
			: "0"};
	color: ${(props) =>
		props.selected
			? props.theme.palette.primary.main
			: props.theme.palette.grey.midDark};
	cursor: pointer;
	background: ${(props) =>
		props.selected
			? props.theme.palette.secondary.light
			: props.theme.palette.grey.light};
	transition: color 0.3s, background 0.3s;
	${(props) => props.theme.breakpoints.down("xs")} {
		font-size: ${(props) => props.theme.typography.s};
	}
`;

function TabSelector() {
	const dispatch = useDispatch();
	const activeTab = useSelector(selectActiveTab);

	const handleTabClick = (num: number) => () => {
		dispatch(tabChanged(num));
	};

	return (
		<Container>
			<StyledTab
				position="left"
				selected={activeTab === 0}
				onClick={handleTabClick(0)}
			>
				Account Settings
			</StyledTab>
			<StyledTab
				position="right"
				selected={activeTab === 1}
				onClick={handleTabClick(1)}
			>
				User Information
			</StyledTab>
		</Container>
	);
}

export default TabSelector;
