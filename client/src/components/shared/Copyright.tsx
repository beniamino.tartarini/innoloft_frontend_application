import React from "react";
import styled from "styled-components";

const StyledSpan = styled.span`
	font-weight: 400;
	font-size: ${(props) => props.theme.typography.xs};
	color: ${(props) => props.theme.palette.text.primary};
	margin: 0.625rem 0;
`;

function Copyright() {
	const date = new Date();

	return <StyledSpan>&copy;{date.getFullYear()} - Innoloft GmbH</StyledSpan>;
}

export default Copyright;
