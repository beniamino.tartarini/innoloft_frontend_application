import React from "react";
import { useSelector } from "react-redux";
import { selectDropdownIsOpen } from "../../features/layout/layoutSlice";
import MenuList from "./MenuList";
import SidebarFooter from "./SidebarFooter";
import styled from "styled-components";

const Container = styled.div`
	position: fixed;
	top: 3.4375rem;
	left: 0;
	height: calc(100vh - 3.4375rem);
	width: 15rem;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	background: ${(props) => props.theme.palette.grey.light};
	box-shadow: ${(props) => props.theme.shadows.dark};
	${(props) => props.theme.breakpoints.up("md")} {
		display: none;
	}
`;

function Dropdown() {
	const isOpen = useSelector(selectDropdownIsOpen);

	return isOpen ? (
		<Container>
			<MenuList slim />
			<SidebarFooter slim />
		</Container>
	) : null;
}

export default Dropdown;
