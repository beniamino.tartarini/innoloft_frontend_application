import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
	display: flex;
	flex: 1;
	max-width: 500px;
	padding-left: 1.25rem;
`;
const Input = styled.input`
	flex: 1;
	border: none;
	border-radius: 5px;
	width: 100%;
	padding: 6px 10px;
	font-size: ${(props) => props.theme.typography.s};
`;

function SearchBox() {
	return (
		<Wrapper>
			<Input placeholder="Enter interest, keyword, company name, etc..." />
		</Wrapper>
	);
}

export default SearchBox;
