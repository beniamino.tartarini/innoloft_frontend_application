import React from "react";
import { Bell } from "@styled-icons/bootstrap/Bell";
import { UserCircle } from "@styled-icons/fa-solid/UserCircle";
import LanguageButton from "./LanguageButton";
import styled from "styled-components";

const ButtonContainer = styled.div`
	display: flex;
	align-items: center;
	padding-left: 0.25rem;
`;
const BellIcon = styled(Bell)`
	color: ${(props) => props.theme.palette.grey.light};
	margin-left: 1rem;
	width: 24px;
	height: 24px;
	${(props) => props.theme.breakpoints.down("sm")} {
		margin-left: 0.5rem;
		width: 20px;
		height: 20px;
	}
`;
const UserCircleIcon = styled(UserCircle)`
	color: ${(props) => props.theme.palette.grey.light};
	width: 24px;
	height: 24px;
	margin-left: 1.5rem;
	${(props) => props.theme.breakpoints.down("sm")} {
		margin-left: 0.75rem;
		width: 20px;
		height: 20px;
	}
`;

function ButtonDiv() {
	return (
		<ButtonContainer>
			<LanguageButton />
			<BellIcon />
			<UserCircleIcon />
		</ButtonContainer>
	);
}

export default ButtonDiv;
