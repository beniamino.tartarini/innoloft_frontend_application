import React from "react";
import { useDispatch, useSelector } from "react-redux";

import styled from "styled-components";
import Hamburger from "./Hamburger";
import SearchBox from "./SearchBox";
import ButtonDiv from "./ButtonDiv";
import Logo from "./Logo";
import {
	dropdownToggled,
	selectDropdownIsOpen,
} from "../../features/layout/layoutSlice";

const Header = styled.header`
	position: fixed;
	top: 0;
	left: 0;
	height: 3.4375rem;
	width: 100%;
	background: ${(props) => props.theme.palette.primary.main};
	box-shadow: ${(props) => props.theme.shadows.base};
	z-index: 10;
`;
const HeaderContainer = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	margin: 0 auto;
	height: 3.4375rem;
	max-width: 92.5rem;
	padding: 0 1.25rem;
	color: ${(props) => props.theme.palette.grey.xLight};
	${(props) => props.theme.breakpoints.down("xs")} {
		padding: 0 0.75rem;
	}
`;

function Navbar() {
	const dispatch = useDispatch();
	const isOpen = useSelector(selectDropdownIsOpen);
	const handleHamburgerClick = () => dispatch(dropdownToggled(!isOpen));
	return (
		<Header>
			<HeaderContainer>
				<Hamburger onClick={handleHamburgerClick} />
				<Logo />
				<SearchBox />
				<ButtonDiv />
			</HeaderContainer>
		</Header>
	);
}

export default Navbar;
