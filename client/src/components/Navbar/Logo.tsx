import React from "react";
import logo from "../../assets/logo.png";
import styled from "styled-components";

const StyledImg = styled.img`
	${(props) => props.theme.breakpoints.down("md")} {
		transform: scale(0.85);
	}
	${(props) => props.theme.breakpoints.down("sm")} {
		display: none;
	}
`;

function Logo() {
	return <StyledImg src={logo} alt="Innoloft logo" />;
}

export default Logo;
