import React from "react";
import styled from "styled-components";
import { ChevronThinDown } from "@styled-icons/entypo/ChevronThinDown";

const Button = styled.div`
	display: flex;
	justify-content: space-around;
	align-items: center;
	background: inherit;
	color: ${(props) => props.theme.palette.grey.light};
	border: none;
	width: 3.125rem;
	margin-left: 0.5rem;
	${(props) => props.theme.breakpoints.down("sm")} {
		margin-left: 0.125rem;
	}
`;
const BtnText = styled.div`
	font-size: ${(props) => props.theme.typography.s};
	font-weight: 300;
	color: inherit;
	${(props) => props.theme.breakpoints.down("xs")} {
		font-size: ${(props) => props.theme.typography.xs};
	}
`;
const BtnIcon = styled(ChevronThinDown)`
	color: inherit;
	width: 16px;
	height: 16px;
	${(props) => props.theme.breakpoints.down("sm")} {
		width: 14px;
		height: 14px;
	}
`;

function LanguageButton() {
	return (
		<Button>
			<BtnText>EN</BtnText>
			<BtnIcon />
		</Button>
	);
}

export default LanguageButton;
