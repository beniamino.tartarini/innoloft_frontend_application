import React, { HTMLAttributes } from "react";
import styled from "styled-components";

interface IMainHeaderProps extends HTMLAttributes<HTMLHeadingElement> {}

const Header = styled.h1`
	font-size: ${(props) => props.theme.typography.xl};
	font-weight: 600;
	color: ${(props) => props.theme.palette.text.secondary};
	margin: 0.9375rem 0;
	${(props) => props.theme.breakpoints.down("xs")} {
		font-size: ${(props) => props.theme.typography.l};
	}
`;

function MainHeader({ children }: IMainHeaderProps) {
	return <Header>{children}</Header>;
}

export default MainHeader;
