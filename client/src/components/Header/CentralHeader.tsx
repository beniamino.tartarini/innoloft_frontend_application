import React from "react";
import MainHeader from "./MainHeader";
import MainHeaderSubtitle from "./MainHeaderSubtitle";

function CentralHeader() {
	return (
		<>
			<MainHeader>A fantastic dashboard</MainHeader>
			<MainHeaderSubtitle>With all you need for business</MainHeaderSubtitle>
		</>
	);
}

export default CentralHeader;
