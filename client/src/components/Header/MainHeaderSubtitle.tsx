import React, { HTMLAttributes } from "react";
import styled from "styled-components";

interface IMainHeaderSubtitleProps extends HTMLAttributes<HTMLHeadingElement> {}

const Subtitle = styled.h2`
	font-size: ${(props) => props.theme.typography.m};
	font-weight: 300;
	color: ${(props) => props.theme.palette.text.primary};
	margin: 0.3125 0.8125;
	${(props) => props.theme.breakpoints.down("xs")} {
		font-size: ${(props) => props.theme.typography.s};
	}
`;

function MainHeaderSubtitle({ children }: IMainHeaderSubtitleProps) {
	return <Subtitle>{children}</Subtitle>;
}

export default MainHeaderSubtitle;
