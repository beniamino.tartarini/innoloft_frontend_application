import React from "react";
import MenuList from "../shared/MenuList";
import SidebarFooter from "../shared/SidebarFooter";
import UserLogin from "./UserLogin";
import styled from "styled-components";

const Container = styled.div`
	position: fixed;
	max-height: calc(100vh - 3.4375rem);
	width: 17.5rem;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	padding: 1.875rem 0 3.125rem;
`;
const ScrollableDiv = styled.div`
	overflow-y: auto;
`;
function Sidebar() {
	return (
		<Container>
			<ScrollableDiv>
				<UserLogin />
				<MenuList />
			</ScrollableDiv>

			<SidebarFooter />
		</Container>
	);
}

export default Sidebar;
