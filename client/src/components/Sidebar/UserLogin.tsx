import React from "react";
import avatar from "../../assets/avatar.jpeg";
import ButtonMain from "../shared/ButtonMain";
import { Enter } from "@styled-icons/ionicons-outline/Enter";
import styled from "styled-components";
const Container = styled.div`
	display: flex;
	align-items: center;
	margin: 0 0 1.875rem 0.625rem;
`;
const ImgWrapper = styled.div`
	height: 6.75rem;
	width: 7.25rem;
	padding-top: 0.25rem;
`;
const EnterIcon = styled(Enter)`
	margin-right: 0.25rem;
	padding-top: 0.125rem;
`;
const Avatar = styled.img`
	height: 6.125rem;
	width: 6.125rem;
	border-radius: 200px;
	border: 1px solid white;
	box-shadow: ${(props) => props.theme.shadows.base};
`;
function UserLogin() {
	return (
		<Container>
			<ImgWrapper>
				<Avatar src={avatar} alt="avatar" />
			</ImgWrapper>
			<ButtonMain slim>
				<EnterIcon size={20} />
				Logout
			</ButtonMain>
		</Container>
	);
}

export default UserLogin;
