import React, { HTMLAttributes } from "react";
import styled from "styled-components";

interface ICardMediaProps extends HTMLAttributes<HTMLImageElement> {
	src: string;
	alt: string;
}

const CardMedia = styled.img`
	height: 100%;
	width: 100%;
	margin-bottom: 0.875rem;
`;

function EventCard({ src, alt }: ICardMediaProps) {
	return <CardMedia src={src} alt={alt} />;
}

export default EventCard;
