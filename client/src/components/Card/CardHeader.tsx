import React, { HTMLAttributes } from "react";
import styled from "styled-components";

interface IHeaderProps extends HTMLAttributes<HTMLHeadingElement> {}

const Header = styled.div`
	font-size: ${(props) => props.theme.typography.m};
	font-weight: 600;
	margin-bottom: 0.5rem;
	color: ${(props) => props.theme.palette.text.secondary};
`;

function EventCard({ children }: IHeaderProps) {
	return <Header>{children}</Header>;
}

export default EventCard;
