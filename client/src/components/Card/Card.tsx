import React, { HTMLAttributes } from "react";
import styled from "styled-components";

interface ICardProps extends HTMLAttributes<HTMLDivElement> {}

const Container = styled.div`
	width: 100%;
	padding: 1.25rem;
	margin-bottom: 1.25rem;
	box-shadow: ${(props) => props.theme.shadows.base};
	background: ${(props) => props.theme.palette.grey.xLight};
`;

function EventCard({ children }: ICardProps) {
	return <Container>{children}</Container>;
}

export default EventCard;
