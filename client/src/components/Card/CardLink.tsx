import React, { HTMLAttributes } from "react";
import styled from "styled-components";

interface ICardLinkProps extends HTMLAttributes<HTMLHtmlElement> {}

const CardLink = styled.a`
	font-size: ${(props) => props.theme.typography.s};
	color: ${(props) => props.theme.palette.text.tertiary};
	cursor: pointer;
	&:hover,
	&:focus {
		color: ${(props) => props.theme.palette.text.primary};
	}
`;

function EventCard({ children }: ICardLinkProps) {
	return <CardLink>{children}</CardLink>;
}

export default EventCard;
