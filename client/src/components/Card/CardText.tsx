import React, { HTMLAttributes } from "react";
import styled from "styled-components";

interface ICardTextProps extends HTMLAttributes<HTMLHtmlElement> {}

const CardText = styled.div`
	font-size: ${(props) => props.theme.typography.s};
	color: ${(props) => props.theme.palette.text.primary};
	margin-bottom: 1rem;
`;

function EventCard({ children }: ICardTextProps) {
	return <CardText>{children}</CardText>;
}

export default EventCard;
