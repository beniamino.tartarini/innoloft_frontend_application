import * as Yup from "yup";

export const lowerCaseRegExp = new RegExp("(?=.*[a-z])");
export const upperCaseRegExp = new RegExp("(?=.*[A-Z])");
export const numberRegExp = new RegExp("(?=.*[0-9])");
export const specialRegExp = new RegExp("(?=.*[!@#$%^&*])");
export const lengthRegExp = new RegExp("(?=.{8,})");
const passwordRegExp = new RegExp("^[a-zA-Z0-9!@#$%^&*^-s]{8,25}$");

// const strictPasswordRegExp = new RegExp(
// 	"^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})"
// );

export const mainSchema = Yup.object().shape({
	email: Yup.string().email().required("Please insert a valid email address"),
	password: Yup.string()
		.matches(passwordRegExp, "Password is not strong enough")
		.required("Please pick a password"),
	passwordConfirmation: Yup.string()
		.oneOf([Yup.ref("password"), undefined], "Passwords must match")
		.required("Password confirmation is required"),
});

export const additionalSchema = Yup.object().shape({
	firstName: Yup.string().required("Required"),
	lastName: Yup.string().required("Required"),
	street: Yup.string(),
	number: Yup.string(),
	city: Yup.string().required("Required"),
	country: Yup.string().required("Required"),
	postCode: Yup.string().required("Required"),
});
