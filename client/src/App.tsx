import React from "react";
import { useWindowWidth } from "@react-hook/window-size";
import Navbar from "./components/Navbar/Navbar";
import Sidebar from "./components/Sidebar/Sidebar";
import EventCard from "./components/shared/EventCard";
import CentralHeader from "./components/Header/CentralHeader";
import Manager from "./features/dashboard/Manager";
import styled from "styled-components";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Dropdown from "./components/shared/Dropdown";

const Grid = styled.div``;
const Container = styled.div`
	display: grid;
	grid-template-columns: [left-col-start]17.5rem[left-col-end main-start]1fr[main-end right-col-start]24rem[right-col-end];
	grid-template-rows: [navbar-start]3.4375rem[navbar-end main-start header-start]max-content[header-end content-start]1fr[content-end main-end];
	max-width: 92.5rem;
	margin: 0 auto;
	padding: 0 1.25rem;
	${(props) => props.theme.breakpoints.down("xl")} {
		grid-template-columns: [left-col-start]17.5rem[left-col-end main-start]1fr[main-end];
	}
	${(props) => props.theme.breakpoints.down("md")} {
		margin: 0 1rem;
		grid-template-columns: [ main-start]1fr[main-end];
	}
	${(props) => props.theme.breakpoints.down("sm")} {
		margin: 0;
	}
`;
const LeftCol = styled.div`
	grid-column: left-col-start/left-col-end;
	grid-row: main-start/-1;
	${(props) => props.theme.breakpoints.down("md")} {
		display: none;
	}
`;
const MainHeader = styled.div`
	grid-column: main-start/main-end;
	grid-row: header-start/header-end;
	margin-left: 1.875rem;
	padding: 1.25rem 0 2rem;
	${(props) => props.theme.breakpoints.down("md")} {
		margin-left: 0;
	}
`;
const MainContainer = styled.div`
	grid-column: main-start/main-end;
	grid-row: content-start/content-end;
	margin-left: 1.875rem;
	padding: 0 0 2rem;
	${(props) => props.theme.breakpoints.down("md")} {
		margin-left: 0;
	}
`;
const RightCol = styled.div`
	grid-column: right-col-start/right-col-end;
	grid-row: content-start/content-end;
	${(props) => props.theme.breakpoints.down("xl")} {
		display: none;
	}
`;
function App() {
	const width = useWindowWidth();

	return (
		<Grid>
			<Container>
				<Navbar />
				<Dropdown />
				{width >= 850 ? (
					<LeftCol>
						<Sidebar />
					</LeftCol>
				) : null}
				<MainHeader>
					<CentralHeader />
				</MainHeader>
				<MainContainer>
					<Manager />
				</MainContainer>
				{width >= 1340 ? (
					<RightCol>
						<EventCard />
					</RightCol>
				) : null}
			</Container>
		</Grid>
	);
}

export default App;
