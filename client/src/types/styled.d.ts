import "styled-components";
export type TSize = "xs" | "sm" | "md" | "lg" | "xl";
type TPaletteScheme = { light: string; main: string; dark: string };
type TPaletteText = {
	primary: string;
	secondary: string;
	tertiary: string;
	white: string;
	label: string;
};
type TPaletteGrey = {
	xLight: string;
	lighter: string;
	light: string;
	mid: string;
	midDark: string;
	base: string;
	dark: string;
	darkest: string;
};
type TTypography = {
	xs: string;
	s: string;
	m: string;
	l: string;
	xl: string;
	xxl: string;
	xxxl: string;
	xxxxl: string;
};
type TShadows = {
	light: string;
	base: string;
	dark: string;
};

declare module "styled-components" {
	export interface DefaultTheme {
		breakpoints: {
			values: {
				xs: string | number;
				sm: string | number;
				md: string | number;
				lg: string | number;
				xl: string | number;
			};
			up: (value: TSize) => string;
			down: (value: TSize) => string;
		};
		palette: {
			primary: TPaletteScheme;
			secondary: TPaletteScheme;
			danger: TPaletteScheme;
			success: TPaletteScheme;
			grey: TPaletteGrey;
			text: TPaletteText;
		};
		typography: TTypography;
		shadows: TShadows;
	}
}
