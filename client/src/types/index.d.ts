declare module "*.png" {
	const value: any;
	export default value;
}
declare module "*.jpeg" {
	const value: any;
	export default value;
}
export type TOptions = { key: string; value: string };
export type TStatus = "idle" | "pending" | "fulfilled" | "rejected";

export interface IDashboardInitialState {
	activeTab: 0 | 1;
	fetchStatus: TStatus;
	updateStatus: TStatus;
	error: string;
	email: string;
	firstName: string;
	lastName: string;
	city: string;
	street: string;
	number: string;
	country: string;
	postCode: string;
}
export interface ILayoutInitialState {
	dropdownIsOpen: boolean;
}
export interface IStore {
	dashboard: IDashboardInitialState;
	layout: ILayoutInitialState;
}
