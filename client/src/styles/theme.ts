import { DefaultTheme } from "styled-components";
const theme: DefaultTheme = {
	breakpoints: {
		values: {
			xs: "25em",
			sm: "37.5em",
			md: "53.125em",
			lg: "63.75em",
			xl: "83.75em",
		},
		up: function (value) {
			return `@media only screen and (min-width: ${theme.breakpoints.values[value]})`;
		},
		down: function (value) {
			return `@media only screen and (max-width: ${theme.breakpoints.values[value]})`;
		},
	},
	palette: {
		primary: { light: "#616797", main: "#272E71", dark: "#1d2253" },
		secondary: { light: "#fdcc1c", main: "#E4B302", dark: "#a68302" },
		danger: { light: "#ff8f8f", main: "#ff4141", dark: "#962a2a" },
		success: { light: "#81c784", main: "#4caf50", dark: "#388e3c" },

		grey: {
			xLight: "#FFF",
			lighter: "#F3F3F3",
			light: "#F6F6F6",
			mid: "#E5E5E5",
			midDark: "#A5A5A5",
			base: "#7e8c89",
			dark: "#454d4b",
			darkest: "#212529",
		},
		text: {
			primary: "#323b49",
			secondary: "#445063",
			tertiary: "#828A97",
			white: "#FFF",
			label: "#161b3e",
		},
	},
	shadows: {
		light: "1px 1px 4px 0 rgba(0, 0, 0, 0.15)",
		base: "1px 1px 6px 0 rgba(0, 0, 0, 0.2)",
		dark: "1px 3px 6px 0 rgba(0, 0, 0, 0.4)",
	},
	typography: {
		xs: "0.75em",
		s: "0.875em",
		m: "1em",
		l: "1.125em",
		xl: "1.313em",
		xxl: "1.5em",
		xxxl: "1.75em",
		xxxxl: "3em",
	},
};

export default theme;
