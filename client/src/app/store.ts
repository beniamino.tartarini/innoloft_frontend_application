import { configureStore } from "@reduxjs/toolkit";
import dashboardReducer from "../features/dashboard/dashboardSlice";
import layoutReducer from "../features/layout/layoutSlice";

export default configureStore({
	reducer: { layout: layoutReducer, dashboard: dashboardReducer },
});
